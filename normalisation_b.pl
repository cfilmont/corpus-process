#!/usr/bin/perl
use strict;
use warnings;
my %h;
my $rep = "C:/Users/cfilmont/Documents/projets/diapason/corpora/datasource/Entities_original/Entities";
opendir(REP,$rep) or die "E/S : $!\n";

while(defined(my $fic=readdir REP)){
	my $f="${rep}/$fic";
    open FIC, "$f" or warn "$f E/S: $!\n";
	my @file=<FIC>;
	foreach my $line (@file){
		$line=~ s/<b>/<b> /g;
		$line=~ s/<\/b>/ <\/b>/g;
		$line=~ s/<i>/<i> /g;
		$line=~ s/<\/i>/ <\/i>/g;
		print "$line";
	}
	close FIC;
}
