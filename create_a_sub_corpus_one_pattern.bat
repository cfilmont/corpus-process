REM autor : Clémence Filmont
REM creation date : 29/07/2016
REM abstract : créer un sous corpus à partir d'un corpus donné. Le filtrage est effectué grâce à un pattern en regex (fichier externe)

setlocal enableDelayedExpansion
@echo off


REM arg1 : chemin du corpus d'entrée (attention, dans cygwin, il faut mettre des "/" comme séparateur de dossier)
set INPUTCORPUSPATH=%1
REM arg2 : profondeur du corpus d'entrée (ex. 3 pour sciences et vie vs. 0 pour diapason)
set DEEPNESS=%2
REM arg3 : chemin du fichier destiné à contenir le chemin des fichiers contenant le pattern (sans l'extension)
set FILTERFILENAME=%3
REM arg4 : chemin du fichier contenant le pattern à extraire
FOR /f "delims=" %%i IN ('cat %4') DO set PATTERN=%%i
REM arg5 : chemin du corpus de sortie
set OUTPUTPATH=%5%
REM arg6 : on décide si on veut restreindre le nombre de fichier
set FILENUMBERVAR=%6%

@echo on
echo "input directory : "%INPUTCORPUSPATH%
echo "deepness of the input directory : "%DEEPNESS%
echo "file containing files containing the pattern : "%FILTERFILENAME%
echo "pattern to filter : "%PATTERN%
echo "output directory : "%OUTPUTPATH%
@echo off
cd %INPUTCORPUSPATH%
REM on récupère le nombre de fichier dans le dossier contenant le corpus
FOR /f "delims=" %%i IN ('ls %INPUTCORPUSPATH% ^|wc -l') DO set FILENB=%%i
if "%FILENB%" == "1" (
	set MULTIPLEXMLFILES=0
) else (
	set MULTIPLEXMLFILES=1
)

REM on crée un fichier qui va contenir le nom du/des fichiers à filtrer
touch "%FILTERFILENAME%".txt
REM on crée le dossier destiné à contenir le sous corpus
mkdir "%OUTPUTPATH%"
REM si le corpus est rassemblé dans un seul fichier
if "%MULTIPLEXMLFILES%" == "0" (
	grep -E "%PATTERN%" *.xml > %OUTPUTPATH%/output.xml
) else (
	if "%FILENUMBERVAR%" == "true" (
		@echo on
		echo "filtering output files"
		@echo off
		REM arg6 : nombre de fichier qu'on veut extraire
		set FILENUMBER=%7%
		REM si le corpus est repartit dans plusieurs fichiers, recherche récursive
		grep -RlE "%PATTERN%" | tail -"!FILENUMBER!" > %FILTERFILENAME%.txt
	) else (
		grep -RlE "%PATTERN%" > %FILTERFILENAME%.txt
	)
	REM pour chaque ligne, donc pour chaque fichier contenant le motif
	FOR /f "delims=^" %%L IN (%FILTERFILENAME%.txt) DO (
		if %DEEPNESS% GEQ 1 (
			FOR /f "tokens=%DEEPNESS% delims=^/" %%j IN ('echo %%L') DO set FILENAME=%%j
			REM ecrit son contenu dans un fichier dedie
			cat %%L > %OUTPUTPATH%/"!FILENAME!"
			@echo on
			echo "processing input corpus directories"
			@echo off
		) else (
			@echo on
			echo "processing input corpus files"
			@echo off
			cat %%L > %OUTPUTPATH%/%%L
		)
	)
)
goto :END