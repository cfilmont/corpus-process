set INPUTCORPUSPATH=%1
cd %INPUTCORPUSPATH%
set FILTERFILENAME=%2
touch %FILTERFILENAME%.txt
set REGEX1=%3
FOR /f "delims=" %%i IN ('cat %4') DO set REGEX2=%%i
set OUTPUTPATH=%5
mkdir "%OUTPUTPATH%"
grep -vE %REGEX1% *.xml |grep -lE %REGEX2% > %FILTERFILENAME%.txt
FOR /f "delims=\r" %%L IN (%FILTERFILENAME%.txt) DO (
	cat %%L > %OUTPUTPATH%/%%L.txt
)