REM autor : Clémence Filmont
REM creation date : 27/07/2016
REM abstract : prétraitement à l'annotation pour les corpus contenu dans un seul fichier xml : splitte du fichier et création d'une arboressence pour chacun d'entre eux
REM : si il y a des body dans le fichier xml d'entrée qu'on veut convertir en div, il faut ajouter le dossier "entities" dans le chemin de sortie en décommentant la ligne 42

@echo off
Setlocal enabledelayedexpansion

REM param1 : chemin du corpus contenant le fichier a splitter
set INPUTCORPUSPATH=%1
REM param2 : nom du fichier xml à splitter (sans l'extension)
set INPUTFILENAME=%2

set ENTETE=^<?xml version^=""1.0"" encoding^=""UTF-8""?^>\r^<result name=""response""^> 
set BALISEFIN=^</result^>
set TEMPORARYFILE=c:/Windows/Temp
cd %TEMPORARYFILE%
mkdir annot
cd %INPUTCORPUSPATH%
REM on enleve l'entete xml du fichier d'origine et on met le fichier d'output dans temp
tail -n +3 "%INPUTFILENAME%".xml > %TEMPORARYFILE%/annot/"%INPUTFILENAME%".tmp
REM on split le fichier en autant de fichier qu'il contient de balise doc et on supprime les fichiers vides
csplit -f file -z %TEMPORARYFILE%/annot/"%INPUTFILENAME%".tmp "/<doc>/" "{*}"
REM on supprime le fichier temporaire
rm %TEMPORARYFILE%/annot/%INPUTFILENAME%.tmp
REM ajoute l'extension xml à la fin des fichiers splittes (l'option -b de csplit ne marche pas)
for %%f in (file*) DO (
	ren %%~nxf %%~nxf.xml
)
for /f "delims=" %%k in ('grep -E "<doc>" "%INPUTFILENAME%".xml ^|wc -l') DO (set INPUTFOLDERLENGTH=%%k)
@echo ON
echo %INPUTFOLDERLENGTH% fichiers dans le dossier
@echo OFF
set /A COUNT=1
set OUTPUTDIR="%INPUTFILENAME%"splitte
mkdir %OUTPUTDIR%
REM pour chaque fichier splitte
for %%i in (file*) DO (
		@echo ON
		echo %%i
		@echo OFF
	REM on ajoute l'entête xml et les balises ouvrantes et fermantes result
	sed -i 1i"%ENTETE%" %%i
	if not !COUNT! == %INPUTFOLDERLENGTH% (
		sed -i $a"%BALISEFIN%" %%i
	)
	REM on récupère son nom
	for /f "delims=. tokens=1" %%j in ('echo %%i') DO (set FOLDERNAME=%%j)
	REM echo !FOLDERNAME!
	REM on recrée l'arboressence nécessaire pour l'execution des script d'annot et de conversion des body en div
	REM set OUTPUTPATH="%OUTPUTDIR%"/!FOLDERNAME!/Entities
	set OUTPUTPATH="%OUTPUTDIR%"/!FOLDERNAME!
	mkdir !OUTPUTPATH!
	mv %%i !OUTPUTPATH!
	set /A COUNT+=1
)

